package wrz.calculatorime;

import java.text.DecimalFormat;
import java.text.ParsePosition;

import com.speqmath.Parser;

import android.content.Context;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Implemntation of all keyboard behavior.
 *
 * @author William R. Zwicky
 */
public class CalculatorIMEService extends InputMethodService {
	private static final char ADD = '+';
	private static final char SUB = '-';
	private static final char MUL = '\u00D7';
	private static final char DIV = '\u00F7';
	
    private KeyboardView mInputView;
    private TextView mCandidateView;
    private StringBuilder mCandidate = new StringBuilder();
    private DecimalFormat e_fmt = new DecimalFormat("#.#######");

    /** expression evaluator */
    Parser evaluator = new Parser();

    
    /**
     * This is the main point where we do our initialization of the input method
     * to begin operating on an application.  At this point we have been
     * bound to the client, and are now receiving all of the detailed information
     * about the target of our edits.
     */
	@Override
	public void onStartInput(EditorInfo attribute, boolean restarting) {
		super.onStartInput(attribute, restarting);
		setCandidatesViewShown(false);
	}

	/**
     * Called by the framework when your view for creating input needs to
     * be generated.  This will be called the first time your input method
     * is displayed, and every time it needs to be re-created such as due to
     * a configuration change.
     */
	@Override
	public View onCreateInputView() {
		if(mInputView == null) {
	        mInputView = (KeyboardView) getLayoutInflater().inflate(
	                R.layout.calculator, null);
	//        mInputView.setOnKeyboardActionListener(this);
	        Keyboard keyboard = new Keyboard(this, R.xml.calculator_keyboard);
	        mInputView.setKeyboard(keyboard);
	        
	        mInputView.setOnKeyboardActionListener(new KeyboardView.OnKeyboardActionListener() {
	        	public void onKey(int primaryCode, int[] keyCodes) {
	        		doKey(primaryCode);
	        	}

	        	public void onPress(int primaryCode) {
	        	}

	        	public void onRelease(int primaryCode) {
	        	}

				public void onText(CharSequence text) {
				}

				public void swipeDown() {
				}

				public void swipeLeft() {
				}

				public void swipeRight() {
				}

				public void swipeUp() {
				}
	        });
		}
        return mInputView;
    }

	/**
	 * Called when the input view is being shown and input has started on a new
	 * editor. This will always be called after onStartInput(EditorInfo,
	 * boolean) and onCreateInputView(), allowing you to do your general setup
	 * there and just view-specific setup here.
	 */
	@Override
	public void onStartInputView(EditorInfo info, boolean restarting) {
		super.onStartInputView(info, restarting);
        InputConnection ic = getCurrentInputConnection();
        mCandidate.setLength(0);
		
		// Collect entire text field.
		int t1, t2;
		try {
			CharSequence t = ic.getTextBeforeCursor(Integer.MAX_VALUE, 0);
            t1 = t.length();
			mCandidate.append(t);
		} catch (IndexOutOfBoundsException e) {
			// happens frequently; docs won't explain why
		    t1 = 0;
		}
		try {
			CharSequence t = ic.getTextAfterCursor(Integer.MAX_VALUE, 0);
			mCandidate.append(t);
			t2 = t.length();
		} catch (IndexOutOfBoundsException e) {
			// happens frequently; docs won't explain why
		    t2 = 0;
		}

		
		//TODO wrong! scan outward from cursor for digits; '+-' if field is signed, '.' if decimal
		
		// Scan for right-most string that looks like a number.
		int start = 0;
		boolean done = false;
		while(!done) {
			try {
				Double.parseDouble(mCandidate.substring(start));
				done = true;
			} catch (NumberFormatException e) {
				start += 1;
				if(start >= mCandidate.length())
					done = true;
			}
		}

		// Delete number, and replace with composing.
		mCandidate.delete(0, start);
		t1 = mCandidate.length() - t2;
		if(t1 < 0) {
			t1 = 0;
			t2 = mCandidate.length();
		}
		
		ic.deleteSurroundingText(t1, t2);
		ic.setComposingText(mCandidate, 1);
	}
	
	@Override
	public void onFinishInput() {
	    // can't do anything; android commits the composing text before we ever get control
//        InputConnection ic = getCurrentInputConnection();
//        ic.setComposingText("", 0);
//    	evaluate();
//    	ic.commitText(mComposing, mComposing.length());
    	mCandidate.setLength(0);
    	setCandidatesViewShown(false);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
	}
    
	@Override
	public View onCreateCandidatesView() {
		if(mCandidateView == null) {
			mCandidateView = new TextView(this);
			
			mCandidateView.setHorizontalFadingEdgeEnabled(true);
			mCandidateView.setHorizontalScrollBarEnabled(false);
			mCandidateView.setVerticalScrollBarEnabled(false);
			mCandidateView.setTextColor(0xFF000000);
			mCandidateView.setBackgroundColor(0xFFEEFFEE);
			mCandidateView.setTextSize(18);
			mCandidateView.setLines(1);
		}
		
		if(mCandidate != null)
		    mCandidateView.setText(mCandidate);
		else
			mCandidateView.setText("");
		
		return mCandidateView;
	}

    /**
     * Deal with the editor reporting movement of its cursor.
     */
	@Override
	public void onUpdateSelection(int oldSelStart, int oldSelEnd,
			int newSelStart, int newSelEnd, int candidatesStart,
			int candidatesEnd) {
		super.onUpdateSelection(oldSelStart, oldSelEnd, newSelStart, newSelEnd,
				candidatesStart, candidatesEnd);
	}
	
	
	
	// ===================================================================== //
	
	
	
	private void doKey(int keyCode) {
		switch(keyCode) {
		case '0':
			mCandidate.append('0');
			break;
		case '1':
			mCandidate.append('1');
			break;
		case '2':
			mCandidate.append('2');
			break;
		case '3':
			mCandidate.append('3');
			break;
		case '4':
			mCandidate.append('4');
			break;
		case '5':
			mCandidate.append('5');
			break;
		case '6':
			mCandidate.append('6');
			break;
		case '7':
			mCandidate.append('7');
			break;
		case '8':
			mCandidate.append('8');
			break;
		case '9':
			mCandidate.append('9');
			break;
		case '.':
			mCandidate.append('.');
			break;

		case Keyboard.KEYCODE_DELETE:
			if(mCandidate.length() > 0)
				mCandidate.setLength(mCandidate.length()-1);
			break;
			
		case '+':
			mCandidate.append(ADD);
			break;
		case '-':
			mCandidate.append(SUB);
			break;
		case '*':
			mCandidate.append(MUL);
			break;
		case '/':
			mCandidate.append(DIV);
			break;
			
		case '=':
        case Keyboard.KEYCODE_DONE:
            try {
                evaluate();
            } catch (com.speqmath.Error e) {
                toast(this, e);
            }
        
            if(keyCode == Keyboard.KEYCODE_DONE) {
            	sendDefaultEditorAction(true);
            }
			break;
		}
		
		display();
	}

    /**
     * Display in composing area if possible, else display best string in
     * composing area, and full string in candidate area.
     */
    private void display() {
        InputConnection ic = getCurrentInputConnection();
        String compo = null;
        String candi = mCandidate.toString();
        
        if(mCandidate.length() == 0) {
            ic.setComposingText(mCandidate, 1);
            setCandidatesViewShown(false);
        }
        else {
            double dval = 0;
            try {
                // try to parse whole string as number
                dval = Double.parseDouble(candi);
                compo = candi;
                candi = null;
            } catch (NumberFormatException e) {
                // mCom is not just a number; extract first number in string
                ParsePosition p = new ParsePosition(0);
                Number n = e_fmt.parse(candi, p);
                if(n != null) {
                    dval = n.doubleValue();
                    if(p.getIndex() > 0)
                        compo = candi.substring(0, p.getIndex());
                }
                // else compo stays null
                // and keep candi
            }

            if(compo != null) {
                EditorInfo info = getCurrentInputEditorInfo();
                if((info.inputType & InputType.TYPE_CLASS_TEXT) != 0
                    || (info.inputType & InputType.TYPE_NUMBER_FLAG_DECIMAL) != 0) {
                    // decimal point is allowed: display entire number
                    ic.setComposingText(compo, 1);
                }
                else {
                    // decimal is forbidden: display rounded number, enable candidate view with entire number
                    long ival = Math.round(dval);
                    ic.setComposingText(Long.toString(ival), 1);
                    candi = mCandidate.toString();
                }
            }
            
            if(candi == null) {
                setCandidatesViewShown(false);
            }
            else {
                // can't parse string at all; leave composing text alone
                mCandidateView.setText(candi);
                setCandidatesViewShown(true);
            }
        }
    }

    /** replace mCom with its evaluation, if possible */
    private void evaluate() {
        if(mCandidate.length() > 0) {
            double v = evaluator.parse(mCandidate.toString());
            mCandidate.setLength(0);
            mCandidate.append(v);
        }
    }

    /** display toast with exception */
    public static void toast(Context context, Throwable t) {
        String msg = t.getMessage();
        if(msg == null || msg.length() == 0)
            msg = t.getClass().getSimpleName();

        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }
}
